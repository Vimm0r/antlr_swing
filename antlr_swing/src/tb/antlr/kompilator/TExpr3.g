tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer j = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name = {$e}, declarations = {$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}? -> setVariable(i = {$i1.text}, j = {$e2.st})
        | ^(EQUALS e1=expr e2=expr) -> rowny(i = {$e1.st}, j = {$e2.st})
        | ^(NOT_EQUALS e1=expr e2=expr) -> rozny(i = {$e1.st}, j = {$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {j++;} -> if(condition = {$e1.st}, t = {$e2.st}, f = {$e3.st}, j = {j.toString()})
        | INT  -> int(i = {$INT.text})
        | ID  {globals.hasSymbol($ID.text)}? -> getVariable(i = {$ID.text})
    ;
    